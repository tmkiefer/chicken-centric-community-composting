# Chicken-centric Community Composting

Community-scale composting, working with the wonderful nature of gallus gallus domesticus, offers carbon sequestration, waste reduction, and food production.